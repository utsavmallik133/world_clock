import 'package:flutter/material.dart';
import 'package:worldtime/services/world_time.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';


class Loading extends StatefulWidget {
  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {


void setupWorldTime() async{
  WorldTime w1 = WorldTime(location: 'Kathmandu', flag: 'kathmandu.png', url: 'Asia/Kathmandu');
  await w1.getTime();
  Navigator.pushReplacementNamed(context, '/home',arguments: {
    'location': w1.location,
    'time': w1.time,
    'flag': w1.flag,
    'isDayTime':w1.isDayTime,
  });
}

  @override
  void initState() {

    super.initState();
    setupWorldTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlue,
      body: Center(
        child:SpinKitCubeGrid(
          color: Colors.amberAccent,
          size: 50.0,
        ),
      ),
    );
  }
}
