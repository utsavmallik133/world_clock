import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  Map data = {};
//  String bgImage;
  @override
  Widget build(BuildContext context) {
    data = data.isNotEmpty ? data : ModalRoute.of(context).settings.arguments; //arguments that we have received
    print(data);
    String bgImage;
    //set background
    if(data['location'] == 'London') {
      bgImage = 'pr_lon.jpg';
    }
    else if(data['location'] == 'Berlin'){
      bgImage  = 'pr_ber.jpg';
    }
    else if(data['location'] == 'Chicago'){
      bgImage  = 'pr_chi.jpg';
    }
    else if(data['location'] == 'New York'){
      bgImage  = 'pr_ny.jpg';
    }
    else if(data['location'] == 'Sydney'){
      bgImage  = 'pr_au.jpg';
    }

    else{
      bgImage = 'pr_ktm.jpg';
    }

//    String bgImage = data['isDayTime'] ? 'pr_day.jpg': 'pr.jpg.png';
    Color bgColor = data['isDayTime'] ? Colors.lightBlue : Colors.grey;
//    print(bgImage);
    return Scaffold(
      backgroundColor: bgColor,
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/$bgImage'),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0.0,120.0,0,0),
            child: Column(
              children: <Widget>[
                FlatButton.icon(
                  onPressed: () async{
                    dynamic result = await Navigator.pushNamed(context, '/location');
                    setState(() {
                        data = {
                          'time' : result['time'],
                          'location' : result['location'],
                          'flag' : result ['flag'],
                          'isDayTime' : result['isDayTime']
                        };
                    });

                  },
                  icon: Icon(
                      Icons.edit_location,
                      color: Colors.white,),
                  label: Text('Edit Location',
                  style : TextStyle(
                    color: Colors.white,
                  )),
                ),
                SizedBox(height: 20.0,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    data['location'],
                    style: TextStyle(
                      fontSize: 28.0,
                      letterSpacing: 2.0,
                      color: Colors.white,

                    ),
                  ),
                ],
              ),
                SizedBox(height: 20.0,),
                Text(data['time'],
                style: TextStyle(
                  fontSize: 66.0,
                  color : Colors.white,
                ),),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
